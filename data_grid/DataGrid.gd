extends Spatial

const Utils = preload("res://tools/utils.gd")

export(String) var image_path = ""
export(float) var bar_height = 100.0

var x_dim
var z_dim

var locations =  []
var mask_bits := []
var multipliers := []
var selected_index := 0
var max_multiplier := 100.0
var min_multiplier := 0.1
var largest_weight := 1.0

onready var mesh = $MultiMeshInstance
onready var code_box = $CanvasLayer/Control/MarginContainer/VBoxContainer/HBoxContainer/Code
onready var year_box = $CanvasLayer/Control/MarginContainer/VBoxContainer/HBoxContainer/Year
onready var code_weight = $"CanvasLayer/Control/MarginContainer/VBoxContainer/HBoxContainer/Code Weight"

func _ready():
	mesh.material_override.set_shader_param("maxHeight", bar_height)
	mesh.multimesh.instance_count = 15
#	mesh.multimesh.instance_count = 39
	x_dim = 15
#	x_dim = 39
	z_dim = 1
	_build_information()
	_build_mesh()
	code_box.get_popup().connect("id_pressed", self, "_on_codeitem_selected")
	year_box.get_popup().connect("id_pressed", self, "_on_yearitem_selected")

func _process(_delta):
	var image = _build_mask()
	var texture = ImageTexture.new()
	texture.create_from_image(image)
	texture.flags = 0
	$CanvasLayer/TextureRect.texture = texture
	mesh.material_override.set_shader_param("selectionMask", texture)

func _build_mesh():
	for x in range(x_dim):
		for z in range(z_dim):
			var ind = z * x_dim + x
			var location = Utils.arr2d_to_vec3(locations[ind]) - Utils.arr2d_to_vec3(Utils.GLOBAL_OFFSET)
			location.y = yield(Utils.sample_global_height(location, get_parent()), "completed")

func _build_information():
	print(get_node("CanvasLayer/Control") is Node)
	var file = File.new()
	file.open("res://data_grid/test.json", File.READ)
	var json = JSON.parse(file.get_line())
	if (json.error == OK):
		var dict = json.result
		mask_bits.resize(dict["sorted_codes"].size())
		for bit in mask_bits.size():
			mask_bits[bit] = false
		fill_code_box(dict["sorted_codes"])
		fill_year_box(dict["sorted_years"])
		locations = dict["locations"]
	

func _build_mask():
	var mask = Image.new()
	
	var mask_array = []
	var idx = 0
	for bit in mask_bits:
		var color = Color(float(bit), map(multipliers[idx], 0.0, multipliers.max(), 0.0, 1.0), 0.0)
		#print(color.g8)
		mask_array.append(color.r8)
		mask_array.append(color.g8)
		mask_array.append(color.b8)
		idx += 1
	
	mask_array = PoolByteArray(mask_array)
	mask.data = {"data" : mask_array, "format" : "RGB8", "height" : 1, "width" : mask_bits.size(), "mipmaps" : false}
	return mask

#absolutely might be redundant, just a small helper
func _apply_new_sampler(sampler, id):
	mesh.material_override.set_shader_param(id, sampler)

func fill_code_box(codes):
	for item in codes:
		code_box.get_popup().add_check_item(item)
		multipliers.append(1.0)
	mesh.material_override.set_shader_param("numCodes", codes.size())
	print(mesh.material_override.get_shader_param("numCodes"))

func fill_year_box(years):
	for item in years:
		year_box.get_popup().add_radio_check_item(str(item))
	mesh.material_override.set_shader_param("numYears", years.size())

func _on_codeitem_selected(index):
	mask_bits[index] = !mask_bits[index]
	code_box.get_popup().set_item_checked(index, mask_bits[index])
	selected_index = index
	code_weight.text = "%.f" % multipliers[selected_index]


func _on_yearitem_selected(index):
	mesh.material_override.set_shader_param("year", index)
	for idx in year_box.get_popup().get_item_count():
		year_box.get_popup().set_item_checked(idx, false)
	year_box.get_popup().set_item_checked(index, true)

func map(value, i_min, i_max, o_min, o_max):
	return lerp(o_min, o_max, inverse_lerp(i_min, i_max, value))


func _on_Code_Weight_text_entered(new_text : String):
	if (!new_text.is_valid_float()):
		code_weight.text = "%.f" % multipliers[selected_index]
	elif (float(new_text) > max_multiplier || float(new_text) < min_multiplier):
		code_weight.text = "%.f" % multipliers[selected_index]
	else:
		multipliers[selected_index] = float(new_text)
		if (float(new_text) > largest_weight):
			mesh.material_override.set_shader_param("maxHeight", bar_height * float(new_text))
