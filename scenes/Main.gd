extends Spatial

export(String, DIR) var building_scenes_directory
export(String, DIR) var lotline_scenes_directory
export(NodePath) var building_parent
export(NodePath) var lotline_parent

# Moves the lot_lines node slightly up to avoid z-fighting with terrain
export(float, 0,10,0.1) var lotlines_height_offset = 0.5

var current_selection = null

func _ready():
	# Currently loads all objects
	# Should be changed to work on demand and on a thread
	_set_lotlines_height_offset(lotlines_height_offset)
	_load_objects(get_node(building_parent), building_scenes_directory, true)
	_load_objects(get_node(lotline_parent), lotline_scenes_directory)

func _load_objects(parent_node:Node, directory:String, clickable:bool = false):
	var files = _get_dir_contents(directory)
	for i in files.size():
		var child_node = load(files[i]).instance()
		parent_node.add_child(child_node)
		if clickable:
			child_node.connect("clicked", self, "_on_building_clicked")
		if not (i+1) % 100:
			yield(get_tree(), "idle_frame")


func _set_lotlines_height_offset(offset):
	get_node(lotline_parent).transform.origin.y = offset

func _get_dir_contents(path) -> PoolStringArray:
	var file_list = PoolStringArray()
	var dir = Directory.new()
	if dir.open(path) == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		while file_name != "":
			if file_name.get_extension() == "scn":
				file_list.append(path + "/" + file_name)
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the path.")
	return file_list

func _on_building_clicked(building):
	current_selection = building
	for p in current_selection.get_property_list():
		print(p)
#	print(current_selection.get_property_list())
	update_building_info()
	
func update_building_info():
	var coords = current_selection.get_meta("coordinates")
	$UI/main/Info/Label.text = "Selected: %s. Coodinates: %s FILENAME: %s" % [current_selection.name, coords, current_selection.filename]
