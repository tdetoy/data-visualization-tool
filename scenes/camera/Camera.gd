extends Camera

enum { IDLE, PRE_DRAG, DRAGGING, ROTATING}

signal clicked(pos)

var state = IDLE

func _unhandled_input(event:InputEvent):
	if event.is_action_pressed("wheel_up"):
		translate_object_local(Vector3.FORWARD * 0.2 * transform.origin.y)
		pass
	if event.is_action_pressed("wheel_down"):
		translate_object_local(Vector3.FORWARD * (-0.2 * transform.origin.y))
	if event.is_action_pressed("l_click"):
		state = PRE_DRAG
	elif event.is_action_released("l_click"):
		if state == PRE_DRAG:
			emit_signal("clicked", event.position)
		state = IDLE
	if event.is_action_pressed("r_click"):
		state = ROTATING
	elif event.is_action_released("r_click"):
		state = IDLE
		
	if event is InputEventMouseMotion:
		match state:
			PRE_DRAG:
				# More than 10 px/s is considered an intentional drag motion
				# Should probably be configurable and also adapt to screen dpi using OS.get_screen_dpi()
				if event.speed.length_squared() > 100:
					state = DRAGGING
					continue
			DRAGGING:
				global_translate(0.005 * transform.origin.y * -Vector3(event.relative.x, 0, event.relative.y).rotated(Vector3.UP,global_transform.basis.get_euler().y))
			ROTATING:
				rotate_object_local(Vector3.RIGHT, 0.003 * event.relative.y)
				global_rotate(Vector3.UP, 0.01 * event.relative.x)
