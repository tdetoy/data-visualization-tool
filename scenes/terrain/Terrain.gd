tool
extends Spatial

const Utils = preload("res://tools/utils.gd")

func _ready():
	_reposition()


func _reposition():
	# bounding box in coodinates
	var begin = [-7497367.70, 2019998.75]
	var end = [-7252464.83, 2102026.67]
	
	# Make sure the mesh is centered
	$Plane.transform.origin = Vector3.ZERO
	
	# the mesh is now centered in the node so we need to find
	# the center coordinate to offset the node position correctly
	var center = [ 0.5 * (begin[0] + end[0]), 0.5 * (begin[1] + end[1]) ]
	
	var offset3d = Utils.arr2d_to_vec3(Utils.GLOBAL_OFFSET)
	var center3d = Utils.arr2d_to_vec3(center)
	global_transform.origin = center3d - offset3d

