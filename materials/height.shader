shader_type spatial;
render_mode specular_schlick_ggx;

uniform vec4 sea : hint_color;
uniform vec4 lo : hint_color;
uniform vec4 mid : hint_color;
uniform vec4 hi : hint_color;

void vertex() {
	vec3 col = vec3(0.0,0.0,0.0);
	if (VERTEX.y < 0.00001) {
			col = sea.rgb;
	} else if (VERTEX.y < 0.001) {
		col = lo.rgb;
	} else if (VERTEX.y < 0.01) {
		col = mid.rgb;
	} else {
		col = hi.rgb;
	}

	COLOR = vec4(col, 1.0);
}

void fragment() {
	ALBEDO = COLOR.rgb;
}
