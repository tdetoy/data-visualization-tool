extends MeshInstance

const mat_normal = preload("res://materials/building.material")
const mat_hover = preload("res://materials/building_hover.material")
const mat_selected = preload("res://materials/building_selected.material")

var selected = false setget set_selected, get_selected

var pre_selected = false

signal clicked(self_ref)

func set_selected(toggled : bool):
	if toggled:
		selected = true
		add_to_group("selection")
		material_override = mat_selected
	else:
		selected = false
		remove_from_group("selection")
		material_override = mat_normal

func get_selected():
	return selected

func _ready():
	if not has_meta("coordinates"):
		set_meta("coordinates", [-1,-1])
	$col.connect("mouse_entered", self, "_on_mouse_entered")
	$col.connect("mouse_exited", self, "_on_mouse_exited")
	$col.connect("input_event", self, "_on_input_event")
	
func _on_mouse_entered():
	if selected:
		return
	material_override = mat_hover

func _on_mouse_exited():
	pre_selected = false
	if selected:
		return
	material_override = mat_normal
	
func _on_input_event(_camera, event, _click_position, _click_normal, _shape_idx):
	if event.is_action_pressed("l_click"):
		pre_selected = true
	if pre_selected and event.is_action_released("l_click"):
		get_tree().set_group("selection", "selected", false)
		call_deferred("set_selected", true)
		emit_signal("clicked", self)
	pass # Replace with function body.
