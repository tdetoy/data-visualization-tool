tool
extends EditorScript

# DEPRECATED


const json_path = "res://json/op_buildings.json"
const model_path = "res://buildings"
#const mat = preload("res://materials/lot_line.material")


func _run():
	var json_file = File.new()
	json_file.open(json_path, File.READ)
	var json_parsed = JSON.parse(json_file.get_as_text())
	
	if json_parsed.error != OK:
		push_error("Parsing json failed with the following error: %s\nAt line: %s" % [json_parsed.error_string, json_parsed.error_line])
		return json_parsed.error
	
	var json_data = json_parsed.result
	var scene = get_scene()
	
	var offset = [-7353208.1559, 2090779.4439999983] # Using first one from plot lines so they align properly
	
	
	for i in json_data.size():
		var coordinates = json_data[i].coordinates[0]
		var vert_0 = Vector2(coordinates[0][0], coordinates[0][1])
		coordinates.pop_back()
		var vertices = PoolVector2Array()
		for vert in coordinates:
			var translated = Vector2(vert[0], vert[1]) - vert_0
			vertices.push_back(translated)
		
		var model : CSGPolygon = CSGPolygon.new()
		model.polygon = vertices
		model.depth = json_data[i].height
		scene.add_child(model)
		model.owner = scene
		
		var pos_3d = Vector3(vert_0.x - offset[0], 0, vert_0.y - offset[1])
		
		model.transform.origin = pos_3d
		model.rotation_degrees.x = 90

