tool
extends EditorScript

const Utils = preload("res://tools/utils.gd")

const SceneScript = preload("res://tools/Building.gd")

const json_path = "res://json/op_buildings.json"
const model_path = "res://buildings"

const save_path = "res://scenes/buildings"

func _run():
	var start_time = OS.get_ticks_msec()
	print(
		"====== ", get_script().get_path(), " ======",
		"\n(RE)GENERATING BUILDINGS"
		)
	
	var scene = get_scene()
	if not scene.get_node_or_null("terrain_collider"):
		printerr(
			"'terrain_collider' not found\n",
			"Make sure an instance of res:scenes/terrain/TerrainCollider.scn\n",
			"exists as Main/terrain_collider. It is required to calculate height"
			)
		return
	
	var json_file = File.new()
	json_file.open(json_path, File.READ)
	var json_parsed = JSON.parse(json_file.get_as_text())
	
	if json_parsed.error != OK:
		push_error("Parsing json failed with the following error: %s\nAt line: %s" % [json_parsed.error_string, json_parsed.error_line])
		return json_parsed.error
	
	var json_data = json_parsed.result

	var offset3d = Utils.arr2d_to_vec3(Utils.GLOBAL_OFFSET)
	
	for i in json_data.size():
		var model_file = load("%s/bldg_zero_%s.glb" % [model_path, i])
		var model_gltf: Spatial = model_file.instance()

		var model : MeshInstance = model_gltf.get_node("bldg_%s" % [i]).duplicate()
		model.material_override = preload("res://materials/building.material")
		model.name = "bldg_%s" % [i]
		
		var absolute_position = Utils.arr2d_to_vec3(json_data[i].coordinates[0][0])
		model.transform.origin = absolute_position - offset3d
		
		# Adjust height so it's not floating or sunk in the terrain
		model.transform.origin.y = Utils.sample_global_height(model.transform.origin, scene)
		
		# Save original coordinates as serializable metadata
		model.set_meta("coordinates", json_data[i].coordinates[0][0])
		
		# Create collider and set it as serializable
		model.create_trimesh_collision()
		var collider_name = "col"
		model.get_node(model.name + "_col").name = collider_name
		model.get_node(collider_name).propagate_call("set_owner", [model])
		
		# Set it to use Building.gd as script
		model.set_script(SceneScript)

		Utils.save_as_scene(model, save_path, model.name)
		
		
		if not (i+1) % 200:
			print_debug(i+1, " of ", json_data.size(), " buildings created..")
			yield(scene.get_tree(), "idle_frame")
		
	json_file.close()
	print_debug(json_data.size(), " buildings created")
	print_debug("Done in ", OS.get_ticks_msec() - start_time, "msec\n======")

