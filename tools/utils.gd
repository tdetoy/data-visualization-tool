extends Node

# The map coordinate that corresponds to Vector3.ZERO
# in in-engine coordinates. Use to calculate the 3D position of
# all objects to avoid huge displacements that might induce
# floating-point presicion errors
# Currently the first point in op_lot_lines.json
const GLOBAL_OFFSET := [-7353208.1559, 2090779.4439999983]

# Converts an array [x,y] where x is longitude and
# y is latitude to a Vector3 where x -> x and y -> -z
# Godot us Y-up, right handed
# https://pbs.twimg.com/media/EmVSW5AW8AAoDD9?format=jpg&name=4096x4096
static func arr2d_to_vec3(pos: Array, height = 0.0):
	return Vector3(pos[0], height, -pos[1])


# Returns collision height in global coordinates
# Passed scene must have colliders already present
# 
static func sample_global_height(global_pos: Vector3, scene: Node, verbose := false) -> float:
	var ret : = 0.0
	var raycast := RayCast.new()
	scene.call_deferred("add_child", raycast)
	yield(scene.get_tree(), "idle_frame")
	raycast.global_transform.origin = global_pos
	raycast.cast_to = Vector3(0,2000,0)
	raycast.force_raycast_update()
	if raycast.is_colliding():
		ret = raycast.get_collision_point().y
		if verbose:
			print("Raycast hit at: ", ret)
	else:
		if verbose:
			printerr("No raycast hit")
	raycast.free()
	return ret

static func save_as_scene(scene: Node, folder:String, filename: String) -> int:
	var packed_scene = PackedScene.new()
	var packing_error = packed_scene.pack(scene)
	if packing_error != OK:
		printerr("Error %s packing scene %s" % [packing_error, filename])
		return packing_error
	var save_path = folder + "/" + filename + ".scn"
	var writing_error = ResourceSaver.save(save_path, packed_scene)
	if writing_error != OK:
		printerr("Error %s saving scene at %s" % [writing_error, save_path] )
	return OK
	

