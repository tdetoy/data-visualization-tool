tool
extends EditorScript

const Utils = preload("res://tools/utils.gd")

const json_path = "res://json/op_lot_lines.json"
const model_path = "res://buildings"
const mat = preload("res://materials/lot_line.material")

const save_path = "res://scenes/lot_lines"

func _run():
	var start_time = OS.get_ticks_msec()
	print(
		"====== ", get_script().get_path(), " ======",
		"\n(RE)GENERATING LOT LINES"
		)
	
	var scene = get_scene()
	if not scene.get_node_or_null("terrain_collider"):
		printerr(
			"'terrain_collider' not found\n",
			"Make sure an instance of res:scenes/terrain/TerrainCollider.scn\n",
			"exists as Main/terrain_collider. It is required to calculate height"
			)
		return
	
	var json_file = File.new()
	json_file.open(json_path, File.READ)
	var json_parsed = JSON.parse(json_file.get_as_text())
	
	if json_parsed.error != OK:
		push_error("Parsing json failed with the following error: %s\nAt line: %s" % [json_parsed.error_string, json_parsed.error_line])
		return json_parsed.error
	
	var json_data = json_parsed.result

	var offset3d = Utils.arr2d_to_vec3(Utils.GLOBAL_OFFSET)
	
	var st = SurfaceTool.new()
	
	for i in json_data.size():
		var coordinates = json_data[i].coordinates[0]
		var vert_0 = Utils.arr2d_to_vec3(coordinates[0])
		
		st.begin(Mesh.PRIMITIVE_LINE_LOOP)
		st.add_color(Color.white)
		st.add_uv(Vector2.ZERO)
		st.add_normal(Vector3.UP)
		for vert in coordinates:
			var translated = Utils.arr2d_to_vec3(vert) - vert_0
			translated.y = Utils.sample_global_height(Utils.arr2d_to_vec3(vert) - offset3d, scene)
			st.add_vertex(translated)
		st.index()
		st.set_material(mat)
		var mesh = st.commit()
		st.clear()
		
		var lot_line : MeshInstance = MeshInstance.new()
		lot_line.mesh = mesh
		lot_line.name = "lotline_%s" % [i]
		lot_line.cast_shadow = false
		
		lot_line.transform.origin = vert_0 - offset3d
		
		#scene.add_child(lot_line)
		#lot_line.owner = scene
		Utils.save_as_scene(lot_line, save_path, lot_line.name)
		
		
		if not (i+1) % 200:
			print_debug(i+1, " of ", json_data.size(), " lot lines created..")
			yield(scene.get_tree(), "idle_frame")
	
	json_file.close()
	print_debug(json_data.size(), " lot lines created")
	print("Done in ", OS.get_ticks_msec() - start_time, "msec\n======")

